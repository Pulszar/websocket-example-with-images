<h1>Project main goal:</h1>
<b>This project is example of websockets with spring boot application. It send to all connected users several messages with json with image encoded as Base64 string.<b>

You can see after the first message from the client there are several responses. This is possible because it is used websocket.


<h2>How to start it:</h2>
After you pull the project please go to com.testwebsocket.websocket.WebsocketApplication and start it as java application. Spring boot will start embeded webcontainer so no need 
to install Tomcat or something else.

<h2>NB: If you want to start this application in external webcontainer then you need to make some changes.</h2>
Please search tutorial how to transform spring boot application to work in external web container like Tomcat.
In this situation from perspective of the application you must change com.testwebsocket.websocket.config.WebSocketConfig name of ApplicationDestinationPrefixes to your web application name.
Also inside index.html you must change the paths with the app and all other paths you have to add the path to your application. 
For example: stompClient.subscribe('/topic/reply' ... will become stompClient.subscribe('Your_Application_Path/topic/reply' ... and so on

<h2>What this application has and what not:</h2>
For the sake of simplicity I did not add Spring Security authontication or anything of the sort.
That why the app is sending to all user the same messages. 

If you want to send messages per user then you must change com.testwebsocket.websocket.controller.MessageController in method
send line essagingTemplate.convertAndSend("/topic/reply", outputMessage). Here you must provide user and also change the paths from /topic/ to /queue/ and in the frontend
the rows for the subscription to:

 stompClient.subscribe("/user/queue/errors", function(message) {
                        alert("Error " + message.body);
                    });
                    
                    
stompClient.subscribe('/user/queue/reply', function(messageOutput) {
                        showMessageOutput(JSON.parse(messageOutput.body));
                    });
                    
<h2>Brief class explanation:</h2>

<b>com.testwebsocket.websocket.WebsocketApplication</b> -> Main class from which you can start the app.

<b>com.testwebsocket.websocket.config.WebSocketConfig</b> -> Spring configuration for websocket.

<b>com.testwebsocket.websocket.controller.MessageController</b> -> Controller which handler the request and response from and to client.

<b>com.testwebsocket.websocket.models.InputMessage</b> -> POJO for the comming messages from the client (browser).

<b>com.testwebsocket.websocket.models.OutputMessage</b> -> POJO for the messages which will be send to the client (browser). Here image is the Base64 string encoded image

<b>/src/main/resources/static/index.html</b> -> Html represent the client.

                    
<h2>Have a nice day and good luck! :)</h2>

