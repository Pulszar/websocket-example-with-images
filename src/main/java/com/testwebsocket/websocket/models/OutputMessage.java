package com.testwebsocket.websocket.models;

public class OutputMessage
{
	private String user;
	private String text;
	private String time;
	private String image;

	public OutputMessage(String user, String text, String time, String image)
	{
		this.user = user;
		this.text = text;
		this.time = time;
		this.image = image;
	}

	public String getUser()
	{
		return user;
	}
	
	public void setUser(String user)
	{
		this.user = user;
	}

	public String getText()
	{
		return text;
	}

	public void setText(String text)
	{
		this.text = text;
	}

	public String getTime()
	{
		return time;
	}

	public void setTime(String time)
	{
		this.time = time;
	}

	public void setImage(String image)
	{
		this.image = image;
	}

	public String getImage()
	{
		return image;
	}
}
