package com.testwebsocket.websocket.models;

public class InputMessage
{
	private String user;
	private String text;
	private Integer count;

	public String getUser()
	{
		return user;
	}

	public void setUser(String user)
	{
		this.user = user;
	}

	public String getText()
	{
		return text;
	}

	public void setText(String text)
	{
		this.text = text;
	}
	
	public Integer getCount()
	{
		return count;
	}
	
	public void setCount(Integer count)
	{
		this.count = count;
	}

}
