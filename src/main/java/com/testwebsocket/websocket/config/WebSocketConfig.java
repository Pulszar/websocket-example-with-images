package com.testwebsocket.websocket.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

/**
 * Implementation of WebSocketMessageBrokerConfigurer. After spring uses java 8
 * and WebSocketMessageBrokerConfigurer has default implementation on the
 * methods there is no need of
 * {@link org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer
 * AbstractWebSocketMessageBrokerConfigurer}
 * 
 * @author skywalker
 *
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer
{

	@Override
	public void configureMessageBroker(MessageBrokerRegistry config)
	{
		/**
		 * "/queue/" will be per specific user and "/topic/" is public
		 */
		config.enableSimpleBroker("/topic/", "/queue/");
		/**
		 * If you use external web container (external Tomcat and so on) then
		 * this must be the name of your application.
		 * 
		 * In this example I use
		 * embedded web container with the spring boot and this name doesn't
		 * mean. Just when you send messages it should be part of the path.
		 * Check See index.html -> sendMessage() method
		 */
		config.setApplicationDestinationPrefixes("/app");
	}

	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry)
	{
		/**
		 * This is the endpoint where client must send. See index.html ->
		 * sendMessage() method implementation
		 */
		registry.addEndpoint("/images").withSockJS();

	}
}