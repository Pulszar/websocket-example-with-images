package com.testwebsocket.websocket.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import com.google.common.base.Preconditions;
import com.testwebsocket.websocket.models.InputMessage;
import com.testwebsocket.websocket.models.OutputMessage;

/**
 * Our controller which will receive and send responses.
 * @author skywalker
 *
 */
@Controller
public class MessageController
{
	@Autowired
	SimpMessagingTemplate messagingTemplate;

	/**
	 * Method which accept the request from the client
	 * @param message
	 * @throws Exception
	 */
	@MessageMapping("/images")
	public void send(InputMessage message) throws Exception
	{
		Preconditions.checkNotNull(message);
		ClassLoader classLoader = getClass().getClassLoader();
		
		/**
		 * Just loop several times to show the push capability of the websocket
		 */
		for (int i = 0; i < message.getCount(); i++)
		{
			OutputMessage outputMessage = createOutputMessage(classLoader, "test.png", message);
			messagingTemplate.convertAndSend("/topic/reply", outputMessage);
			Thread.sleep(1000);
		}

	}
	
	/**
	 * Simple method for creating one OutputMessage
	 * @param classLoader - can not be null
	 * @param fileName - can not be null
	 * @param message - can not be null
	 * @return OutputMessage object
	 */
	private static OutputMessage createOutputMessage(ClassLoader classLoader, String fileName, InputMessage message)
	{
		Preconditions.checkNotNull(classLoader);
		Preconditions.checkNotNull(fileName);
		Preconditions.checkNotNull(message);
		String time = new SimpleDateFormat("HH:mm:ss").format(new Date());
		
		File file = new File(classLoader.getResource(fileName).getFile());
		String encodstring = encodeFileToBase64Binary(file);

		OutputMessage outputMessage = new OutputMessage(message.getUser(), message.getText(), time, encodstring);
		return outputMessage;
	}

	/**
	 * Read and return content of a file as Base64 encoded String
	 * @param file - can not be null. Must exist.
	 * @return String object.
	 */
	private static String encodeFileToBase64Binary(File file)
	{
		Preconditions.checkNotNull(file);
		Preconditions.checkArgument(file.exists());

		String encodedfile = null;
		try (FileInputStream fileInputStreamReader = new FileInputStream(file))
		{
			byte[] bytes = new byte[(int) file.length()];
			fileInputStreamReader.read(bytes);
			encodedfile = new String(Base64.getEncoder().encode(bytes), "UTF-8");
		} catch (IOException e)
		{
			throw new RuntimeException(e);
		}
		return encodedfile;
	}

	/**
	 * Error handler method. If error occur it will send it to the user
	 * @param exception
	 * @return String object
	 */
	@MessageExceptionHandler
	@SendTo("/topic/errors")
	public String handleException(Throwable exception)
	{
		return exception.getMessage();
	}
}