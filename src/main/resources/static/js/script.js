var stompClient = null;
             
function setupConnected(connected) {
    document.getElementById('connect').disabled = connected;
    document.getElementById('disconnect').disabled = !connected;
    if(connected)
    {
    	document.getElementById('message-div').style.visibility = 'visible';
    }
    else
    {
    	document.getElementById('message-div').style.visibility = 'hidden';
    }
    document.getElementById('response').innerHTML = '';
}
 
function connect() {
    var socket = new SockJS('/images');
    stompClient = Stomp.over(socket);  
    stompClient.connect({}, function(frame) {
    	setupConnected(true);
        
        console.log('Connected: ' + frame);
        
        stompClient.subscribe("/topic/errors", function(message) {
            alert("Error: " + message.body);
        });
        
        
        stompClient.subscribe('/topic/reply', function(messageOutput) {
            showMessageOutput(JSON.parse(messageOutput.body));
        });
    });
}
 
function disconnect() {
    if(stompClient != null) {
        stompClient.disconnect();
    }
    setupConnected(false);
    console.log("Disconnected");
}
 
function sendMessage() {
    var user = document.getElementById('user').value;
    var text = document.getElementById('text').value;
    var count = document.getElementById('count').value;
    
    stompClient.send("/app/images", {}, JSON.stringify({'user':user, 'text':text, 'count': count}));
}
 
function showMessageOutput(messageOutput) {
    var response = document.getElementById('response');
    var p = document.createElement('p');
    p.appendChild(document.createTextNode(messageOutput.user + ": " + messageOutput.text + " (" + messageOutput.time + ")"));
    response.appendChild(p);
    
    var img = document.createElement('img');
    img.setAttribute('src', "data:image/png;base64," + messageOutput.image);
    response.appendChild(img);
}

window.onbeforeunload = function () {
	disconnect();
}

